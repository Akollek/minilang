###############################
# ast_nodes.py                #
# --------------------------- #
#   Author: Amiel Kollek      #
#   ID: 260507214             #
# --------------------------- #
#   This file contains all    #
# the classes used in the     #
# minilang complier           #
###############################

######################
#     AST NODES      #
######################

class ASTNode(object):
    def __init__(self, lineno):
        self.lineno = lineno

#============#
# Statements #
#============#

# Declarations
# ------------

class Decl(ASTNode):
    def __init__(self, lineno, name, _type):
        super(Decl, self).__init__(lineno)
        self.name = name
        self._type = _type

    def __repr__(self):
        return "<Decl {n} ({t})>".format(n=self.name, t=self._type)

    # used for pretty print
    def __str__(self):
        return 'var {n}: {t};\n'.format(n=self.name,t=self._type)

    def C(self):
        dec_type = self._type if self._type != 'string' else 'char *'
        return '{t} {n};\n'.format(t=dec_type,n=self.name)


# Assignment node
# ---------------

class AssgnStmt(ASTNode):
    def __init__(self, lineno, name, expr):
        super(AssgnStmt, self).__init__(lineno)
        self.name = name
        self.expr = expr

    # used for pretty print
    def __str__(self):
        return '{n} = {e};\n'.format(n=self.name,e=str(self.expr))

    def C(self):
        return '{n} = {e};\n'.format(n=self.name,e=self.expr.C())

# Control flow nodes
# ------------------

# TODO: Make this a meta class
class FlowStmt(ASTNode):
    def __init__(self, lineno, test):
        # TODO: Typecheck test?
        super(FlowStmt, self).__init__(lineno)
        self.test = test

class IfStmt(FlowStmt):
    def __init__(self, lineno, test, if_stmts, else_stmts=None):
        super(IfStmt, self).__init__(lineno, test)
        self.if_stmts = if_stmts
        self.else_stmts = else_stmts

    def __repr__(self):
        if self.else_stmts:
            return '<IfElseStmt ({l})>'.format(l=self.lineno)
        else:
            return '<IfStmt ({l})>'.format(l=self.lineno)
    
    def __str__(self):
        code = 'if ({t}) then\n\t'.format(t=self.test)
        code += '\t'.join(map(str,self.if_stmts))
        if self.else_stmts:
            code += 'else\n\t'
            code += '\t'.join(map(str,self.else_stmts))
        code += 'endif\n\n'
        return code

    def C(self):
        code = 'if ({t}) {{\n\t'.format(t=self.test.C())
        code += '\t'.join([ stmt.C() for stmt in self.if_stmts])
        if self.else_stmts:
            code += '} else {\n\t'
            code += '\t'.join([ stmt.C() for stmt in self.else_stmts])
        code += '}\n'
        return code 

class WhileStmt(FlowStmt):
    def __init__(self, lineno, test, stmts):
        super(WhileStmt, self).__init__(lineno, test)
        self.stmts = stmts

    def __repr__(self):
        return '<WhileStmt ({l})>'.format(l=self.lineno)

    def __str__(self):
        code = 'while ({t}) do\n\t'.format(t=self.test)
        code += '\t'.join(map(str,self.stmts))
        code += 'done\n\n'
        
        return code

    def C(self):
        code = 'while ({t}) {{'.format(t=self.test.C())
        code += '\t'.join([ stmt.C() for stmt in self.stmts ])
        code += '}\n'
        
        return code


# IO Nodes

# --------

class PrintStmt(ASTNode):
    def __init__(self, lineno, expr):
        super(PrintStmt, self).__init__(lineno)
        self.expr = expr

    def __repr__(self):
        return '<PrintStmt ({l})>'.format(l=self.lineno)

    def __str__(self):
        return 'print {e};\n\r'.format(e=str(self.expr));

    def C(self):
        printf_str = {
                'int'   : 'd',
                'float' : 'f',
                'string': 's'
            }[self.expr._type]

        return 'printf("%'+printf_str+'\\n",'+self.expr.C()+');\n'

class ReadStmt(ASTNode):
    def __init__(self, lineno, name):
        super(ReadStmt, self).__init__(lineno)
        self.name = name

    def __repr__(self):
        return '<ReadStmt: {n} ({l})>'.format(n=self.name,l=self.lineno)

    def __str__(self):
        return 'read {n};\n'.format(n=self.name);

    def C(self):
        if self.name._type == 'string':
            return 'read_string('+self.name.C()+');\n'
        else:
            scanf_str = 'd' if self.name._type == 'int' else 'f'
            return 'scanf("%'+scanf_str+'",&'+self.name.C()+');\n'

#=============#
# Expressions #
#=============#

class Expr(ASTNode):
    def __init__(self, lineno, _type):
        super(Expr, self).__init__(lineno)
        self._type = _type

class BinopExpr(Expr):
    ops = {
            '+' : 'PLUS',
            '-' : 'MINUS',
            '*' : 'TIMES',
            '/' : 'DIVIDE',
            }

    def __init__(self, lineno, op, left, right):
        # we don't know the type yet, so init with None
        super(BinopExpr, self).__init__(lineno, None)
        self.op = self.ops[op]
        self.left = left
        self.right = right

        # for pretty printing
        self.op_str = op

    def __repr__(self):
        return '<Expr: {l} {o} {r}>'.format(l=self.left,o=self.op,r=self.right)

    def __str__(self):
        return '({l}) {o} ({r})'.format(l=self.left,o=self.op_str,r=self.right)
        
    def C(self):
        right = self.right.C()
        left  = self.left.C()
        if self._type == 'string':
            if self.op == 'MINUS':
                right = 'string_uminus({r})'.format(r=right)
            return 'string_add({l},{r})'.format(l=left,r=right)
        else:
            return '({l}) {o} ({r})'.format(l=left,o=self.op_str,r=right)

class UMinusExpr(Expr):
    def __init__(self, lineno, expr):
        super(UMinusExpr, self).__init__(lineno, expr._type)
        self.expr = expr

    def __repr__(self):
        return '<Expr: -{e}>'.format(e=repr(self.expr))

    def __str__(self):
        return '-({e})'.format(e=str(self.expr))

    def C(self):
        if self.expr._type == 'string':
            return 'string_uminus({e})'.format(e=self.expr.C())
        else:
            return '-({e})'.format(e=self.expr.C())

class LitExpr(Expr):
    def __init__(self, lineno, value, _type):
        super(LitExpr, self).__init__(lineno, _type)
        self.value = value

    def __repr__(self):
        return '<LitExpr: {v} ({t})>'.format(v=self.value, t=self._type)

    def __str__(self):
        if self._type == 'string':
            return '"'+self.value+'"'
        else:
            return str(self.value)
    
    def C(self):
        if self._type == 'string':
            return '"'+self.value+'"'
        else:
            return str(self.value)
        
    # unary minus
    #def __neg__(self):
    #    return LitExpr(-self.value, self._type)

class NameExpr(Expr):
    def __init__(self, lineno, name):
        # since we don't know type on the first pass,
        # we init with a None type
        super(NameExpr, self).__init__(lineno, None)
        self.name = name

    def __repr__(self):
        return '<NameExpr: {n}>'.format(n=self.name)

    def __str__(self):
        return self.name

    def C(self):
        return self.name


