###############################
# compiler.py                 #
# --------------------------- #
#   Author: Amiel Kollek      #
#   ID: 260507214             #
# --------------------------- #
#   This file contains the    #
# class for the minilang      # 
# compiler.                   #
###############################


# I'm using ply: http://www.dabeaz.com/ply/ply.html
import ply.lex as lex
import ply.yacc as yacc
import os.path

# get the AST node classes
from ast_nodes import *

#import logging
#logging.basicConfig(
#    level = logging.DEBUG,
#    filename = "parselog.txt",
#    filemode = "w",
#    format = "%(filename)10s:%(lineno)4d:%(message)s"
#)
#log = logging.getLogger()



#####################
#     COMPILER      #
#####################



class MinilangCompiler(object):
    
    # class for the symbol table
    class Symbol(object):
        def __init__(self, _type):
            self._type  = _type

        def __repr__(self):
            return '<Symbol ({})>'.format(self._type)

        def __str__(self):
            return self._type

    # class for error messages
    class ErrorMsg(object):
        def __init__(self, lineno, message):
            self.lineno = str(lineno)
            self.message = message

        def __repr__(self):
            return '<Error: line {l}>'.format(l=self.lineno)

        def __str__(self):
            return 'ERROR Line {l}: {m}'.format(l=self.lineno, m=self.message)

    # Scanning code 
    # -------------

    tokens = (
            'PLUS',
            'MINUS',
            'TIMES',
            'DIVIDE',
            'EQUALS',
            'PRINT',
            'READ',
            'IF',
            'THEN',
            'ELSE',
            'ENDIF',
            'WHILE',
            'DO',
            'DONE',
            'NAME',
            'VAR',
            'COLON',
            'INTEGER',
            'FLOAT',
            'STRING',
            'INTEGER_LIT',
            'FLOAT_LIT',
            'STRING_LIT',
            'LPAREN',
            'RPAREN',
            'SEMICOLON',
            )
    
    reserved_words = {
            r'print' : 'PRINT',
            r'read'  : 'READ',
            r'if'    : 'IF',
            r'then'  : 'THEN',
            r'else'  : 'ELSE',
            r'endif' : 'ENDIF',
            r'while' : 'WHILE',
            r'do'    : 'DO',
            r'done'  : 'DONE',
            r'int'   : 'INTEGER',
            r'float' : 'FLOAT',
            r'string': 'STRING',
            r'var'   : 'VAR'
            } 
    
    t_PLUS   = r'\+'
    t_MINUS  = r'-'
    t_TIMES  = r'\*'
    t_DIVIDE = r'/'
    t_EQUALS = r'='
    t_COLON  = r':'
    t_LPAREN = r'\('
    t_RPAREN = r'\)'
    t_SEMICOLON = ';'
    
    # match all words as names, then catch if it is a reserved word
    def t_NAME(self, t):
        r'[a-zA-Z_][a-zA-Z0-9_]*'
        t.type = self.reserved_words.get(t.value,'NAME')
        return t
    
    # try and match float before int (otherwise int always matches)
    def t_FLOAT_LIT(self, t):
        r'([1-9][0-9]*|0)?\.[0-9]*'
        t.value = float(t.value)
        return t
    
    def t_INTEGER_LIT(self, t):
        r'([1-9][0-9]*|0)'
        t.value = int(t.value)
        return t
    
    def t_STRING_LIT(self, t):
        r'"[a-zA-Z0-9\s.!?]*"'
        # strip away quotation marks
        t.value = t.value[1:-1]
        return t
    
    def t_COMMENT(self, t):
        r'\#.*'
        pass
    
    def t_newline(self, t):
        r'\n+'
        t.lexer.lineno += len(t.value)
    
    def t_WHITESPACE(self, t):
        r'(\s|\t)+'
        pass
    
    def t_error(self, t):
        print "ERROR: Illegal syntax '{0}' at line {1}".format(t.value[0],
                                                               t.lexer.lineno)
        t.lexer.skip(1)
    
    def t_eof(self, t):
        return None

    # scanning code end
    # -----------------

    # Parsing code
    # ---------------

    precedence = (
        ('left', 'PLUS', 'MINUS'),
        ('left', 'TIMES', 'DIVIDE'),
        ('right', 'UMINUS'),
    )


    def p_program(self, p):
        "program : decllst stmtlst"
        self.dec_list  = p[1]
        self.stmt_list = p[2]


    def p_decllst_decl(self, p):
        """
        decllst : decllst decl
                | decl
                |
        """
        if len(p) == 3:
            p[0] = p[1] + [p[2]]
        else:
            p[0] = [p[1]]


    def p_decl(self, p):
        'decl : VAR NAME COLON type SEMICOLON'
        p[0] = Decl(p.lineno(0), p[2], p[4])

    def p_type(self, p):
        '''
        type : INTEGER
             | FLOAT
             | STRING
        '''
        p[0] = p[1]

    def p_stmtlst_stmt(self, p):
        """
        stmtlst : stmtlst stmt
                | stmt
        """
        if len(p) == 3:
            p[0] = p[1] + [p[2]]
        else:
            p[0] = [p[1]]

    
    def p_stmt_assignment(self, p):
        'stmt : NAME EQUALS expr SEMICOLON'
        # typecheck
        p[0] = AssgnStmt(p.lineno(0), p[1], p[3])

    def p_ifelsestmt(self, p):
        'stmt : IF expr THEN stmtlst ELSE stmtlst ENDIF'
        p[0] = IfStmt(p.lineno(0), p[2],p[4],p[6])

    def p_ifstmt(self, p):
        'stmt : IF expr THEN stmtlst ENDIF'
        p[0] = IfStmt(p.lineno(0),p[2],p[4])

    def p_whilestmt(self, p):
        'stmt : WHILE expr DO stmtlst DONE'
        p[0] = WhileStmt(p.lineno(0), p[2],p[4])

    def p_print(self, p):
        'stmt : PRINT expr SEMICOLON'
        p[0] = PrintStmt(p.lineno(0), p[2])

    def p_read(self, p):
        'stmt : READ expr SEMICOLON'
        p[0] = ReadStmt(p.lineno(0), p[2])

    def p_strexpr(self, p):
        'expr : STRING_LIT'
        p[0] = LitExpr(p.lineno(0), p[1], 'string')

    def p_intexpr(self, p):
        'expr : INTEGER_LIT'
        p[0] = LitExpr(p.lineno(0), p[1], 'int')

    def p_floatexpr(self, p):
        'expr : FLOAT_LIT'
        p[0] = LitExpr(p.lineno(0), p[1], 'float')

    def p_expr_group(self, p):
        'expr : LPAREN expr RPAREN'
        p[0] = p[2]

    def p_numexpr_op(self, p):
        """
        expr : expr PLUS expr
             | expr MINUS expr
             | expr TIMES expr
             | expr DIVIDE expr
             | MINUS expr %prec UMINUS
        """
        if len(p) == 4:
            p[0] = BinopExpr(p.lineno(0), p[2],p[1],p[3])
        elif len(p) == 3:
            p[0] = UMinusExpr(p.lineno(0), p[2])

    def p_expr_name(self, p):
        'expr : NAME'
        p[0] = NameExpr(p.lineno(0),p[1])

     
    def p_error(self, p):
        self.valid = False
        if p:
            self.errors.append(MinilangCompiler.ErrorMsg(p.lineno,
                "Syntax error at {v}".format(v=p.value)))
        else:
            self.errors.append("Syntax error at EOF")

    # parsing code end
    # ----------------


    # Methods for Compiler object
    # ---------------------------

    def __init__(self, filename):
        self.symbols = { }
        self.dec_list  = []
        self.stmt_list = []

        self.valid = True
        self.parsed = False
        self.symbol_table_built = False
        self.typechecked = False
        self.errors = []
        self.filename = filename
        self.prg_dir = os.path.dirname(self.filename)
        self.prg_file = os.path.basename(self.filename)
        # strip away the extension
        self.prg_basename = ''.join(self.prg_file.split('.')[0:-1])
 
    def lex_and_parse(self):
        '''First step in the compliation proccess
        This method reads in the program file, and builds an AST'''

        self.lexer = lex.lex(module=self)
        with open(self.filename,'r') as f:
            self.lexer.input(f.read())

        self.parser = yacc.yacc(module=self,start="program",debug=0)
        self.parser.parse(lexer=self.lexer,tracking=True)
        self.parsed = True


    def build_symbol_table(self):
        '''This pass goes over the declaration list and builds a symbol table'''

        if not self.parsed:
            # can't build a symbol table until we've parsed
            return

        for dec in self.dec_list:
            if dec.name in self.symbols.keys():
                self.valid = False
                self.errors.append(
                    MinilangCompiler.ErrorMsg(dec.lineno,
                         'double declaration of {n}'.format(n=dec.name))
                    )
                continue
            self.symbols[dec.name] = MinilangCompiler.Symbol(dec._type) 
            self.symbol_table_built = True

    def weed(self):
        '''This pass goes over the statement list looking for undeclared variables
        and type checking'''

        if not self.symbol_table_built:
            # can't typecheck until we have a symbol table
            return

        def check(node):
            '''A genereric check method, which calls specific check methods
            on different types of AST nodes'''
            checkers = {
                    AssgnStmt : check_assgn,
                    IfStmt    : check_if,
                    WhileStmt : check_while,
                    BinopExpr : check_binop,
                    UMinusExpr: check_uminus,
                    NameExpr  : check_name,
                    PrintStmt : check_print,
                    ReadStmt  : check_read
            }

            checker = checkers.get(node.__class__,empty_check)
            checker(node)
        
        ## TypeCheckers for different Node types ##
        def empty_check(node):
            pass

        def check_assgn(assgn):
            symbol = self.symbols.get(assgn.name,None)
            if not symbol:
                self.valid = False
                self.errors.append(
                    MinilangCompiler.ErrorMsg(assgn.lineno,
                         'assigning an undecalred variable {n}'.format(n=assgn.name))
                    )
            else:
                check(assgn.expr)
                if assgn.expr._type != symbol._type:
                    self.valid = False
                    self.errors.append(
                        MinilangCompiler.ErrorMsg(assgn.lineno,
                             'assigning {n} of type {nt} to expression of type {et}'.format(
                                 n=assgn.name,
                                 nt=symbol._type,
                                 et=assgn.expr._type))
                             )
                else:
                    # valid
                    pass

        def check_if(ifelse):
            check(ifelse.test)
            if ifelse.test._type != 'int':
                self.valid = False
                self.errors.append(MinilangCompiler.ErrorMsg(ifelse.lineno,
                    'if condition isn\'t an integer'))
            for stmt in ifelse.if_stmts:
                check(stmt)
            if ifelse.else_stmts:
                for stmt in ifelse.else_stmts:
                    check(stmt)

        def check_while(whilestmt):
            check(whilestmt.test)
            if whilestmt.test._type != 'int':
                self.valid = False
                self.errors.append(MinilangCompiler.ErrorMsg(whilestmt.lineno,
                    'while condition isn\'t an integer'))

        def check_name(name):
            symbol = self.symbols.get(name.name, None)
            if not symbol:
                self.valid = False
                self.errors.append(MinilangCompiler.ErrorMsg(name.lineno,
                    'reference to undeclared variable {n}'.format(n=name.name)))
            name._type = symbol._type

        def check_uminus(uminus):
            check(uminus.expr)
            uminus._type = uminus.expr._type

        def check_binop(binop):
            check(binop.left)
            check(binop.right)

            types_as = {
                    ('int','int')       : 'int',
                    ('float','float')   : 'float',
                    ('int','float')     : 'float',
                    ('float','int')     : 'float',
                    ('string','string') : 'string'
                    }
            types_md = {
                    ('int','int')     : 'int',
                    ('float','float') : 'float',
                    ('int','float')   : 'float',
                    ('float','int')   : 'float',
                    }

            if binop.op == 'PLUS' or binop.op == 'MINUS':
                if (binop.right._type,binop.left._type) not in types_as.keys():
                    self.valid = False
                    self.errors.append(MinilangCompiler.ErrorMsg(binop.lineno,
                        'incompatible types for {op}'.format(op=binop.op_str))
                        )
                else:
                    binop._type = types_as[(binop.right._type,binop.left._type)]
            else: # mult/divide
                if (binop.right._type,binop.left._type) not in types_md.keys():
                    self.valid = False
                    self.errors.append(MinilangCompiler.ErrorMsg(binop.lineno,
                        'incompatible types for {op}'.format(op=binop.op_str))
                        )
                else:
                    binop._type = types_md[(binop.right._type,binop.left._type)]
        
        def check_print(printstmt):
            check(printstmt.expr)

        def check_read(readstmt):
            check(readstmt.name)

        ## ----------------------------------------- ##
     
        # checking definitions over, check each statement
        for stmt in self.stmt_list:
            check(stmt)

        self.typechecked = True

    def generate_code(self):
        '''Generates C code.
        C help gotten here: http://stackoverflow.com/questions/8465006/how-to-concatenate-2-strings-in-c
        and here: '''

        if not self.typechecked:
            # can't generated code until typechecked
            return

        code = """// Minilang Generated Code. There could be serious adverse effects if you edit this.
// ... probably not though, but there COULD be.
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define MAX_STRING_SIZE 256
char buffer[MAX_STRING_SIZE];
void read_string(char *var){
    int len;
    scanf("%s",buffer);
    len = strlen(buffer);
    var = (char *) malloc(len+1);
    strcpy(var, buffer);}
char *string_add(char *a, char *b){
    int len1 = strlen(a);
    int len2 = strlen(b);
    char* ret = (char*) malloc(len1+len2+1);
    strcpy(ret,a);
    strcat(ret,b);
    return ret;}
char *string_uminus(char *str){
    int len = strlen(str);
    int i = 1;
    char * ret = (char *) malloc(len+1);
    while(i <= len){ret[i-1] = str[len-i]; i++;}
    return ret;}
"""
        program_code = ''.join( map(lambda x: x.C(),self.dec_list + self.stmt_list))
        code += 'int main(){\n'
        code += program_code
        code += '\nreturn 0;\n}\n'

        cprog_fname = self.prg_basename + '.c'

        with open(cprog_fname, 'w+') as f:
            f.write(code)


    def compile_to_c(self):
        try:
            self.lex_and_parse()
            self.build_symbol_table()
            self.weed()
        except IndexError:
            #   This occurs when the AST is not properly formed
            # due to a syntax error
            # The lexer will record the error
            self.valid = False
        if self.valid:
            self.generate_code()
        else:
            print "Compliation Error"
            for error in self.errors:
                print error

    def prettyprint(self):
        '''Print the minilang program from the AST'''
        if not self.parsed:
            #can't pretty print until parsed
            return

        prettyprint_fname = self.prg_basename + '.pretty.min'
        with open(prettyprint_fname,'w+') as f:
            f.write(''.join(map(str,self.dec_list + ['\n'] + self.stmt_list)))

    def print_symbol_table(self):
        '''Print the symbol table'''
        symboltable_fname = self.prg_basename + '.symbol.txt'

        with open(symboltable_fname,'w+') as f:
            f.write('\n'.join(
                [ '{0}: {1}'.format(k,str(v))
                    for k, v in self.symbols.iteritems()
                ]))

